package com.epam.tasks.dateApi;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

//Дана строка вида "28 Февраль 2015" или “28 February 2015” (*"28 февраля 2015").
// Необходимо сконвертировать ее в вид “28/фев/15” или "28/Feb/15". Локаль учитывать не нужно (* нужно).
// Использовать SimpleDateFormat или DateTimeFormatter
public class ConvertDate {


    private String getFormatDateUS(String date) {
        //Конвертируем строку в дату
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd MMMM yyyy", Locale.US);
        LocalDate localDate = LocalDate.parse(date, dateTimeFormatter);
        //Преобразовываем дату в строку
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("dd/MMM/yy", Locale.US);
        return localDate.format(timeFormatter);
    }

    private String getFormatDateRU(String date) {
        //Конвертируем строку в дату
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd MMMM yyyy", new Locale("ru"));
        LocalDate localDate = LocalDate.parse(date, dateTimeFormatter);
        //Преобразовываем дату в строку
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("dd/MMM/yy", new Locale("ru"));
        return localDate.format(timeFormatter);
    }

    public static void main(String[] args) {
        String dateUS = "28 February 2015";
        String dateRU = "28 февраля 2015";
        System.out.println(new ConvertDate().getFormatDateRU(dateRU));
        System.out.println(new ConvertDate().getFormatDateUS(dateUS));
    }
}
