package com.epam.tasks.dateApi;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class DifferentDate {
    public static void main(String[] args) {
        String dateOne = "31.05.1995";
        String dateTwo = "21.12.2017";

        System.out.print(new DifferentDate().differentDate(dateOne, dateTwo));
    }

    private long differentDate(String dateOne, String dateTwo) {
        DateTimeFormatter dateTimeFormatter1 = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate date1 = LocalDate.parse(dateOne, dateTimeFormatter1);

        DateTimeFormatter dateTimeFormatter2 = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate date2 = LocalDate.parse(dateTwo, dateTimeFormatter2);

        return Math.abs(ChronoUnit.DAYS.between(date2, date1));
    }
}
