package com.epam.tasks.dateApi;

//Определить ваш возраст с момента рождения (можно нафантазировать,
//если нет точных данных =) ) на момент запуска программы. Возраст в секундах, минутах, часах, днях, месяцах и годах

import java.time.*;

public class OldYear {
    public static void main(String[] args){
        LocalDateTime oldLocalDateTime = LocalDateTime.of(1995, Month.MAY, 31, 2, 15, 40);
        LocalDateTime nowLocalDateTime = LocalDateTime.now();

        Duration duration = Duration.between(oldLocalDateTime, nowLocalDateTime);
        Period p = Period.between(oldLocalDateTime.toLocalDate(), nowLocalDateTime.toLocalDate());
        System.out.println("You are " + p.getYears() + " years, or " + p.toTotalMonths() +
                " months or " + duration.toDays() +
                " days old or " + duration.toHours() +
                " hours old or " + duration.toMinutes() +
                " minutes old or " + duration.toMillis() +
                " millis old.");
    }
}
