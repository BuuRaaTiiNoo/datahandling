package com.epam.tasks.others;

public class GetMinMax {
    private String getMinMax(int a, int b, int c){
        return "min value: " + Math.min(Math.min(a, b), c) + "\nmax value: " + Math.max(Math.max(a, b), c);
    }

    public static void main(String[] args){
        System.out.println(new GetMinMax().getMinMax(2, 1,4) + "\n");
        System.out.println(new GetMinMax().getMinMax(7, 3,0));
    }
}
