package com.epam.tasks.regEx;


public class PlaceHolders {
    public static void main(String[] args) {
        String s = "Уважаемый, $userName, извещаем вас о том, что на вашем счете $paymentAccount " +
                "скопилась сумма, превышающая стоимость $numberMonths месяцев пользования нашими услугами. \n" +
                "Деньги продолжают поступать. Вероятно, вы неправильно настроили автоплатеж. С уважением, " +
                "$employee $userPosition";
        s = replaceStr(s, "userName", "****** ******");
        s = replaceStr(s, "paymentAccount", "****************");
        s = replaceStr(s, "numberMonths", "*");
        s = replaceStr(s, "employee", "********* ********** **********");
        s = replaceStr(s, "userPosition", "**********");
        System.out.println(s);
    }

    private static String replaceStr(String s, String templateKey, String templateValue) {
        s = s.replaceAll("\\$" + templateKey + "\\,?", templateValue);
        return s;
    }
}

