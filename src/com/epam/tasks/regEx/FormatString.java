package com.epam.tasks.regEx;

import java.util.Arrays;
import java.util.Comparator;

public class FormatString {

    private String modString(String s) {
        s = s.replace(" ", "\n")
                .replaceAll("[^A-Za-zА-Яа-я-\n]", "")
                .toLowerCase();
        return s;
    }

    private String maxLength(String str) {
        return sortedString(str)[sortedString(str).length - 1];
    }

    private String minLength(String str) {
        return sortedString(str)[0];
    }

    private String[] sortedString(String str) {
        String[] strings = modString(str).split("\n");
        Arrays.sort(strings, Comparator.comparing(s -> String.valueOf(s.length())));
        return strings;
    }

    public static void main(String[] args) {
        String s = "Ребе, Ви случайно не знаете, сколько тогда Иуда получил по нынешнему курсу?";
        System.out.println(new FormatString().modString(s));
        System.out.println("\nСамое короткое слово: " + new FormatString().minLength(s));
        System.out.println("Самое длинное слово: " + new FormatString().maxLength(s));
    }
}
